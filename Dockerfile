FROM mysql:5.6

MAINTAINER Junior Vasquez <jvasqueze33@gmail.com>

COPY 01.-Scripts.sql /docker-entrypoint-initdb.d
